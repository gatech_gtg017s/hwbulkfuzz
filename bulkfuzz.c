// hwbulkfuzz -- fuzzer harness for CCID over libusb_bulk_transfer()
//
// Copyright (C) 2022  Trevor Bentley
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <pthread.h>
#include <libusb.h>
#include <libudev.h>

#define FUZZY 1

#if defined(FUZZY)
#define TARGET_VENDOR      0x9990
#define TARGET_VENDOR_STR  "9990"
#endif
#define TARGET_PRODUCT     0xffff
#define TARGET_PRODUCT_STR NULL

// Fuzzer entry-points
#define ID_USB_BULK_CCID  0xE0

#define CCID_TIMEOUT_MS (5000)
static uint8_t bulk_response[1024*64];

static int fuzzing_seed;
static uint8_t fuzz_applet_id;

static int g_running = 1;
static int g_device_connected = 0;
static pthread_cond_t udev_cond;
static pthread_mutex_t udev_mutex;
static pthread_t udev_thread;
static libusb_device_handle *usb_dev_handle = NULL;
static uint8_t ccid_ep;

static libusb_device_handle *find_device(uint16_t vendor, uint16_t product) {
  struct libusb_device_descriptor dev_desc;
  libusb_device **list = NULL;
  libusb_device_handle *dev_handle = NULL;
  size_t device_count;

  if ((device_count = libusb_get_device_list(NULL, &list)) == 0) {
    fprintf(stderr, "ERROR: libusb_get_device_list\n");
    return NULL;
  }

  for (int i = 0; i < device_count; i++) {
    if (libusb_get_device_descriptor(list[i], &dev_desc) < 0) {
      fprintf(stderr, "ERROR: libusb_get_device_descriptor\n");
      continue;
    }
    if ((vendor == 0xFFFF || dev_desc.idVendor == vendor) &&
        (product == 0xFFFF || dev_desc.idProduct == product)) {
      if (libusb_open(list[i], &dev_handle) < 0) {
        fprintf(stderr, "ERROR: libusb_open\n");
      }
      break;
    }
  }

  return dev_handle;
}

static int find_ccid_endpoints(libusb_device_handle *dev_handle, uint8_t *ep_addr) {
  struct libusb_device_descriptor dev_desc;
  struct libusb_config_descriptor *conf_desc = NULL;
  libusb_device *dev = NULL;
  int i, k;
  uint8_t found = 0;

  if ((dev = libusb_get_device(dev_handle)) == NULL) {
    fprintf(stderr, "ERROR: failed to get device from handle.\n");
    return -1;
  }
  if (libusb_get_device_descriptor(dev, &dev_desc) < 0) {
    fprintf(stderr, "ERROR: failed to open USB descriptor\n");
    return -1;
  }
  printf("Device vendor: 0x%.4x / product: 0x%.4x / configs: %u\n",
         dev_desc.idVendor, dev_desc.idProduct, dev_desc.bNumConfigurations);

  if (libusb_get_config_descriptor(dev, 0, &conf_desc) < 0) {
    fprintf(stderr, "ERROR: libusb_get_config_descriptor\n");
    return -1;
  }

  for (i = 0; i < conf_desc->bNumInterfaces; i++) {
    const struct libusb_interface *iface = &conf_desc->interface[i];
    const struct libusb_interface_descriptor *setting = &iface->altsetting[0];
    for (k = 0; k < setting->bNumEndpoints; k++) {
      const struct libusb_endpoint_descriptor *ep = &setting->endpoint[k];
      if (setting->bInterfaceClass == 0x0b && // CCID class
          ep->bmAttributes == 0x02) {         // Bulk transfer endpoint
        if (ep->bEndpointAddress & 0x80) {    // IN (device->host)
          *ep_addr = ep->bEndpointAddress & ~0x80;
          found++;
        }
        else {                                // OUT (host->device)
          *ep_addr = ep->bEndpointAddress & ~0x80;
          found++;
        }
      }
    }
  }
  return found == 2 ? 0 : -1;
}

static int ccid_icc_power_on(libusb_device_handle *dev_handle, uint8_t ep) {
  int bulk_len = 0;
  //int i;
  uint8_t bulk_resp[2048] = {0};
  if (libusb_bulk_transfer(dev_handle, ep | LIBUSB_ENDPOINT_OUT, (unsigned char*)"\x62\x00\x00\x00\x00\x00\x01\x00\x00\x00", 10, &bulk_len, CCID_TIMEOUT_MS) < 0) {
    fprintf(stderr, "warning: ICC failed (write)\n");
    return -1;
  }
  if (libusb_bulk_transfer(dev_handle, ep | LIBUSB_ENDPOINT_IN, bulk_resp, 2048, &bulk_len, CCID_TIMEOUT_MS) < 0) {
    fprintf(stderr, "warning: ICC failed (read)\n");
    return -1;
  }
  if (bulk_len < 10) {
    fprintf(stderr, "warning: ICC failed (response: %u)\n", bulk_len);
    return -1;
  }
  //printf("icc-on in: ");
  //for (i = 0; i < bulk_len; i++) {
  //  printf("%02x", bulk_resp[i]);
  //}
  //printf("\n");
  return 0;
}

static int ccid_abort(libusb_device_handle *dev_handle, uint8_t ep) {
  int bulk_len = 0;
  uint8_t bulk_resp[2048] = {0};
  unsigned char *abort = (unsigned char*)"\x72\x00\x00\x00\x00\x00\x01\x00\x00\x00";

  if (libusb_bulk_transfer(dev_handle, ep | LIBUSB_ENDPOINT_OUT, abort,
                           10, &bulk_len, CCID_TIMEOUT_MS) < 0) {
    fprintf(stderr, "warning: CCID Abort failed (write)\n");
    return -1;
  }
  if (libusb_bulk_transfer(dev_handle, ep | LIBUSB_ENDPOINT_IN, bulk_resp, 2048, &bulk_len, CCID_TIMEOUT_MS) < 0) {
    fprintf(stderr, "warning: CCID Abort failed (read)\n");
    return -1;
  }
  if (bulk_len < 10) {
    fprintf(stderr, "warning: CCID Abort failed (response: %u)\n", bulk_len);
    return -1;
  }
  return 0;
}

static int ccid_wait_response(libusb_device_handle *dev_handle, uint8_t ep) {
  int bulk_len = 0;
  size_t offset = 0;
  struct timespec start, now;
  uint64_t start_ms, now_ms;
  uint8_t wtx = 100 + 1; // 300ms * 100 = 30s
  //uint8_t wtx = 20; // 300ms * 20 = 6s
  uint16_t wtx_timeout_ms = 350;
  uint8_t got_wtx = 0;

  clock_gettime(CLOCK_REALTIME, &start);

  //fprintf(stdout, "reading ccid response...\n");
  while (wtx) {
    start_ms = ((uint64_t)start.tv_sec * 1000L) + ((uint64_t)start.tv_nsec / 1000 / 1000);

    offset = 0;
    fflush(stdout);
    do {
      if (libusb_bulk_transfer(dev_handle, ep | LIBUSB_ENDPOINT_IN,
                               bulk_response + offset, 4096,
                               &bulk_len, wtx_timeout_ms) != 0) {
        break;
      }
      offset += bulk_len;
    } while(bulk_len == 4096);

    if (offset == 10 && bulk_response[7] == 0x80 && bulk_response[8] == 1) {
      // this is a WTX, keep waiting
      if (!got_wtx) {
        fprintf(stdout, "Got WTX, waiting");
        fflush(stdout);
        got_wtx = 1;
      }
      else {
        fprintf(stdout, ".");
        fflush(stdout);
      }
      wtx--;
      if (--wtx == 0) {
        fprintf(stdout, "\nWTX timeout!\n");
        fflush(stdout);
        return -1;
      }
      clock_gettime(CLOCK_REALTIME, &start);
      continue;
    }
    else if (offset == 0) {
      // timed out with no data and no wtx.  maybe a stuck packet, try pushing garbage
      uint8_t buf[256] = {0};
      clock_gettime(CLOCK_REALTIME, &now);
      now_ms = ((uint64_t)now.tv_sec * 1000L) + ((uint64_t)now.tv_nsec / 1000 / 1000);
      if (got_wtx) fprintf(stdout, "\n");
      fprintf(stdout, "WARNING: ccid timed out, trying dummy bytes...\n");
      fflush(stdout);
      while (now_ms - start_ms < CCID_TIMEOUT_MS) {
        if (libusb_bulk_transfer(dev_handle, ep | LIBUSB_ENDPOINT_OUT, buf, sizeof(buf), &bulk_len, wtx_timeout_ms) < 0) {
          fprintf(stderr, "ERROR: unable to write dummy bytes\n");
          return -1;
        }
        if (libusb_bulk_transfer(dev_handle, ep | LIBUSB_ENDPOINT_IN,
                                 bulk_response + offset, 4096,
                                 &bulk_len, 100) == 0) {
          if (bulk_len > 0) {
            wtx = 0;
            break;
          }
        }
        clock_gettime(CLOCK_REALTIME, &now);
        now_ms = ((uint64_t)now.tv_sec * 1000L) + ((uint64_t)now.tv_nsec / 1000 / 1000);
      }
      fprintf(stdout, "WARNING: ccid exited via dummy bytes\n");
      fflush(stdout);
    }
    else {
      if (got_wtx) fprintf(stdout, "\n");
      wtx = 0;
    }
  }

  return offset;
}

#if defined(WITH_MAIN)
static int ccid_sel_config(libusb_device_handle *dev_handle, uint8_t ep) {
  int bulk_len = 0;
  int i;
  uint8_t bulk_resp[2048] = {0};
  unsigned char* sel_config = (unsigned char*)"\x6f\x0d\x00\x00\x00\x00\x02\x00\x00\x00\x00\xa4\x04\x00\x08\xa0\x00\x00\x11\x22\x33\x44\x55";
  printf("sel out: ");
  for (i = 0; i < 10+11; i++) {
    printf("%02x", sel_config[i]);
  }
  printf("\n");
  usleep(100000ULL);
  libusb_bulk_transfer(dev_handle, ep | LIBUSB_ENDPOINT_OUT, sel_config, 10+13, &bulk_len, 0);
  usleep(100000ULL);
  libusb_bulk_transfer(dev_handle, ep | LIBUSB_ENDPOINT_IN, bulk_resp, 2048, &bulk_len, 0);
  printf("sel in: ");
  for (i = 0; i < bulk_len; i++) {
    printf("%02x", bulk_resp[i]);
  }
  printf(" (%d)\n", bulk_len);
  bulk_resp[bulk_len] = 0;
  printf("Select response: %s\n", bulk_resp + 10);
  return 0;
}

static int ccid_echo_extended(libusb_device_handle *dev_handle, uint8_t ep) {
  int bulk_len = 0;
  int i;
  uint8_t bulk_cmd[3072+10+7] = {0};
  uint8_t bulk_resp[64] = {0};
  int len = 3064;
  int ccid_len = len + 7;
  int total_len = 10 + 7 + len;
  memcpy(bulk_cmd, (unsigned char*)"\x6f\x0d\x00\x00\x00\x00\x03\x00\x00\x00\x00\x01\x00\x00", 14);
  bulk_cmd[1] = (ccid_len & 0xff);
  bulk_cmd[2] = ((ccid_len >> 8) & 0xff);
  bulk_cmd[14] = 0x00;
  bulk_cmd[15] = (len >> 8) & 0xff;
  bulk_cmd[16] = len & 0xff;
  memset(bulk_cmd + 17, 0xAA, len);
  printf("sel out: ");
  for (i = 0; i < total_len; i++) {
    if (i && i % 16 == 0) {
      printf("\n");
    }
    printf("0x%02x,", bulk_cmd[i]);
  }
  printf(" (%d)\n", bulk_len);

  libusb_bulk_transfer(dev_handle, ep | LIBUSB_ENDPOINT_OUT, bulk_cmd, total_len, &bulk_len, 0);
  libusb_bulk_transfer(dev_handle, ep | LIBUSB_ENDPOINT_IN, bulk_resp, 64, &bulk_len, 0);
  printf("sel in: ");
  for (i = 0; i < bulk_len; i++) {
    printf("%02x", bulk_resp[i]);
  }
  printf(" (%d)\n", bulk_len);

  do {
    // this returns max 64, and 0 for ZLPs
    libusb_bulk_transfer(dev_handle, ep | LIBUSB_ENDPOINT_IN, bulk_resp, 4096, &bulk_len, 0);
    printf("sel in: ");
    for (i = 0; i < bulk_len; i++) {
      printf("%02x", bulk_resp[i]);
    }
    printf(" (%d)\n", bulk_len);
  } while (bulk_len == 4096);
  return 0;
}
#endif

void *udev_monitor_thread(void *arg) {
  struct udev *udev = NULL;
  struct udev_monitor *mon = NULL;
  struct udev_device *udev_dev;
  char *cur_devname = NULL;
  int fd;

  udev = udev_new();
  mon = udev_monitor_new_from_netlink(udev, "udev");
  udev_monitor_enable_receiving(mon);
  udev_monitor_filter_add_match_subsystem_devtype(mon, "usb", NULL);
  fd = udev_monitor_get_fd(mon);

  while (g_running) {
    fd_set fds;
    struct timeval tv;
    int ret;

    FD_ZERO(&fds);
    FD_SET(fd, &fds);
    tv.tv_sec = 1;
    tv.tv_usec = 0;
    ret = select(fd+1, &fds, NULL, NULL, &tv);

    if (ret > 0 && FD_ISSET(fd, &fds)) {
      udev_dev = udev_monitor_receive_device(mon);
      if (udev_dev) {
        const char *action = udev_device_get_action(udev_dev);
        const char *devname = udev_device_get_sysname(udev_dev);
        const char *vendor = udev_device_get_sysattr_value(udev_dev, "idVendor");
        const char *product = udev_device_get_sysattr_value(udev_dev, "idProduct");
        if (action && memcmp(action, "bind", 4) == 0 &&
            vendor && ((TARGET_VENDOR_STR == NULL) || (memcmp(vendor, TARGET_VENDOR_STR, 4) == 0)) &&
            product && ((TARGET_PRODUCT_STR == NULL) || (memcmp(product, TARGET_PRODUCT_STR, 4) == 0))) {
          if (cur_devname) free(cur_devname);
          cur_devname = strdup(udev_device_get_sysname(udev_dev));
          printf("device connected!\n");
          g_device_connected = 1;
          pthread_cond_signal(&udev_cond);
        }
        else if (action && memcmp(action, "remove", 6) == 0 &&
                 cur_devname && devname && strcmp(cur_devname, devname) == 0) {
          free(cur_devname);
          cur_devname = NULL;
          g_device_connected = 0;
          printf("device disconnected!\n");
          pthread_cond_signal(&udev_cond);
        }

        udev_device_unref(udev_dev);
      }
    }
  }

  udev_monitor_unref(mon);
  udev_unref(udev);
  return 0;
}

static int fuzz_usb_bulk_ccid(const uint8_t *data, size_t size) {
  uint16_t apdu_len, data_len;
  int retry_count;
  int bulk_len = 0;
  int res;
  int offset = 0;
  if (size < 10 || size > sizeof(bulk_response))
    return -1;

  if (size > 3072+10) {
    return -1;
  }

  apdu_len = size - 10;
  memcpy(bulk_response, data, size);
  bulk_response[1] = (apdu_len >>  0) & 0xff;
  bulk_response[2] = (apdu_len >>  8) & 0xff;
  bulk_response[3] = (apdu_len >> 16) & 0xff;
  bulk_response[4] = (apdu_len >> 24) & 0xff;

  if (apdu_len >= 5 && apdu_len <= 255) {
    // short Lc
    data_len = apdu_len - 5;
    bulk_response[14] = data_len;
  }
  else if (apdu_len >= 7) {
    // long Lc
    data_len = apdu_len - 7;
    bulk_response[14] = 0;
    bulk_response[15] = (data_len >> 8) & 0xff;
    bulk_response[16] = data_len & 0xff;
  }

  // send command
  //printf(" - libusb xfer (len: %ld)\n", size);

  //printf("sel out: ");
  //for (i = 0; i < size; i++) {
  //  printf("%02x", bulk_response[i]);
  //}
  //printf(" (%ld)\n", size);

  if ((res = libusb_bulk_transfer(usb_dev_handle, ccid_ep | LIBUSB_ENDPOINT_OUT,
                                  (unsigned char *)bulk_response, size,
                                  &bulk_len, CCID_TIMEOUT_MS)) == 0) {
    // read response
    offset = ccid_wait_response(usb_dev_handle, ccid_ep);
    if (offset < 0) {
      fprintf(stderr, "ERROR: no response, abort!\n");
      g_device_connected = 0;
      abort();
    }
    //fprintf(stdout, " - ccid response: %u bytes\n", offset);
    //fflush(stdout);
  }
  else {
    // TODO: reset key?  when does a bulk TX fail?
    fprintf(stderr, "ERROR: libusb xfer failed: %d\n", res);
    g_device_connected = 0;
    abort();
  }

#if 0
  // wait for device to crash
  struct timespec ts;
  clock_gettime(CLOCK_REALTIME, &ts);
  ts.tv_nsec += 10000000UL;
  if (ts.tv_nsec > 999999999UL) {
    ts.tv_nsec -= 999999999UL;
    ts.tv_sec += 1;
  }
  pthread_cond_timedwait(&udev_cond, &udev_mutex, &ts);
#endif

  retry_count = 5;
  do {
    if (ccid_abort(usb_dev_handle, ccid_ep) < 0) {
      continue;
    }
    // Sent an ICCPowerOff.  Turn it back on.
    if (data[0] == 0x63) {
      if (ccid_icc_power_on(usb_dev_handle, ccid_ep) < 0) {
        continue;
      }
    }
    break;
  } while (--retry_count);

  if (retry_count == 0) {
    fprintf(stderr, "ERROR: failed to prepare for next round!\n");
    g_device_connected = 0;
    abort();
  }

  if (!g_device_connected) {
    fprintf(stderr, "ERROR: device reset during transaction!\n");
    abort();
  }
  return 0;
}

#if defined(SECONDARY)
int LLVMFuzzerTestOneInputHw(const uint8_t *data, size_t size) {
#else
int LLVMFuzzerTestOneInput(const uint8_t *data, size_t size) {
#endif
  while (!g_device_connected) {
    fprintf(stderr, "NOTICE: no device, waiting...\n");
    pthread_cond_wait(&udev_cond, &udev_mutex);
  }

  switch (fuzz_applet_id) {
  case ID_USB_BULK_CCID:
  default:
    return fuzz_usb_bulk_ccid(data, size);
    break;
  }
  return -1;
}

#if defined(SECONDARY)
int LLVMFuzzerInitializeHw(int *argc, char ***argv) {
#else
int LLVMFuzzerInitialize(int *argc, char ***argv) {
#endif
  static int initialized;
  int i;
  int applet_id_arg = 0;

  // libfuzzer option parsing
  for (i = 0; i < *argc; i++) {
    if (memcmp((*argv)[i], "-seed=", 6) == 0) {
      fuzzing_seed = strtol((*argv)[i] + 6, NULL, 10);
      continue;
    }
    else if (memcmp((*argv)[i], "--applet-idx=", 11) != 0) {
      continue;
    }
    applet_id_arg = strtol((*argv)[i] + 13, NULL, 10);
  }
  fprintf(stderr, "Fuzzing CCID applet: %d\n", applet_id_arg);

  // one-time initialization
  if (!initialized) {
    g_device_connected = 0;
    pthread_cond_init(&udev_cond, NULL);
    pthread_mutex_init(&udev_mutex, NULL);
    pthread_create(&udev_thread, NULL, udev_monitor_thread, NULL);

    if (libusb_init(NULL) < 0) {
      fprintf(stderr, "ERROR: libusb_init\n");
      return 1;
    }

    if ((usb_dev_handle = find_device(TARGET_VENDOR, TARGET_PRODUCT)) == NULL) {
      printf("Waiting for device...\n");
      while (!g_device_connected) {
        pthread_cond_wait(&udev_cond, &udev_mutex);
      }

      if ((usb_dev_handle = find_device(TARGET_VENDOR, TARGET_PRODUCT)) == NULL) {
        fprintf(stderr, "ERROR: didn't find a device.\n");
        return 1;
      }
    }
    else {
      g_device_connected = 1;
    }

    if (find_ccid_endpoints(usb_dev_handle, &ccid_ep) < 0) {
      fprintf(stderr, "ERROR: didn't find CCID endpoints\n");
      return 1;
    }

    if (ccid_icc_power_on(usb_dev_handle, ccid_ep) < 0) {
      fprintf(stderr, "ERROR: icc power on failed!\n");
      abort();
    }
    if (ccid_abort(usb_dev_handle, ccid_ep) < 0) {
      fprintf(stderr, "ERROR: ccid abort failed!\n");
      abort();
    }

    initialized = 1;
  }

  fuzz_applet_id = applet_id_arg;

  return 0;
}

#if defined(WITH_MAIN)
#include <fcntl.h>

int main(int argc, char **argv) {
  int fake_argc = 0;
  ssize_t len;
  uint8_t bytes[4096];
  if (argc != 2) {
    fprintf(stderr, "usage: bulkfuzz <testcase>\n");
    exit(1);
  }
  LLVMFuzzerInitialize(&fake_argc, &argv);

  int fd;
  if ((fd = open(argv[1], O_RDONLY)) < 0) {
    fprintf(stderr, "failed to open testcase: %s\n", argv[1]);
    exit(1);
  }
  if ((len = read(fd, bytes, sizeof(bytes))) < 0) {
    fprintf(stderr, "failed to read testcase: %s\n", argv[1]);
    exit(1);
  }
  ccid_sel_config(usb_dev_handle, ccid_ep);
  ccid_echo_extended(usb_dev_handle, ccid_ep);
  printf("Fuzzing one test case, length %ld\n", len);
  LLVMFuzzerTestOneInput(bytes, len);
  close(fd);

  g_running = 0;
  if (usb_dev_handle) {
    libusb_close(usb_dev_handle);
  }
  libusb_exit(NULL);
  return 0;
}
#endif
